import { AppDataSource } from "./data-source";
import { Product } from "./entity/Product";

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const productRepository = AppDataSource.getRepository(Product)
    // const product = new Product()
    // product.name = "ข้าวหน้าเป็ด";
    // product.price = 50;

    // await AppDataSource.manager.save(product)
    // console.log("Saved a new user with id: " + product.id)

    console.log("Loading products from the database...")
    const products = await productRepository.find()
    console.log("Loaded products: ", products)

    const updateedProduct = await productRepository.findOneBy({id: 1})
    console.log(updateedProduct)
    updateedProduct.price = 80
    await productRepository.save(updateedProduct)

}).catch(error => console.log(error))
